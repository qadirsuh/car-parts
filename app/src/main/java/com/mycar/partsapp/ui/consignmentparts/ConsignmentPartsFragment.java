package com.mycar.partsapp.ui.consignmentparts;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mycar.partsapp.InsertIConsignmentItemActivity;
import com.mycar.partsapp.InsertItemActivity;
import com.mycar.partsapp.R;
import com.mycar.partsapp.SqliteHelper;
import com.mycar.partsapp.Utils;
import com.mycar.partsapp.adapters.ConsignmentCursorAdapter;
import com.mycar.partsapp.adapters.ItemsCursorAdapter;
import com.mycar.partsapp.models.ConsignmentItem;
import com.mycar.partsapp.models.Item;
import com.mycar.partsapp.ui.home.HomeViewModel;

public class ConsignmentPartsFragment extends Fragment implements PopupMenu.OnMenuItemClickListener {

    ConsignmentViewModel consignmentViewModel;

    ListView consignmentListView;

    //Declaration SqliteHelper
    SqliteHelper sqliteHelper;

    ConsignmentCursorAdapter mConsignmentCursorAdapter;
    Cursor mCursor;
    Cursor selectedCursor;

    String itemSearchQuery = "";

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private TextView txtItemsCount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        sqliteHelper = new SqliteHelper(getActivity());
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        consignmentViewModel =
                ViewModelProviders.of(this).get(ConsignmentViewModel.class);
        View root = inflater.inflate(R.layout.fragment_consignment, container, false);
        consignmentListView = root.findViewById(R.id.consignmentListView);
        txtItemsCount = root.findViewById(R.id.txtConsignmentItemsCount);

        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(getActivity(), InsertIConsignmentItemActivity.class);
                mIntent.putExtra("TYPE", "INSERT");
                startActivity(mIntent);

            }
        });

        consignmentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedCursor = (Cursor) consignmentListView.getItemAtPosition(position);
                showPopup(view);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", "Called");
        refreshListData(itemSearchQuery);
    }

    private void refreshListData(String searchQuery) {

        mCursor = sqliteHelper.getAllConsignments(searchQuery);
        mConsignmentCursorAdapter = new ConsignmentCursorAdapter(getActivity(), mCursor, 0);
        consignmentListView.setAdapter(mConsignmentCursorAdapter);

        // Items Count
        String textTotalParts = "Total parts: " + sqliteHelper.getTotalConsignmentPartsCount();
        txtItemsCount.setText(textTotalParts);

    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v, Gravity.RIGHT);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            searchView.setQueryHint("Search Consignment");
            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);
                    itemSearchQuery = newText;
                    refreshListData(newText);

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    itemSearchQuery = query;
                    refreshListData(query);
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.pmnuEdit:
                // Toast.makeText(getActivity(), "You clicked: " + item.getTitle(), Toast.LENGTH_SHORT).show();

                Integer _idEdit = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_ITEMS));
                //String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));
                ConsignmentItem mConsignmentItem = sqliteHelper.getConsignmentItem(_idEdit);
                Log.e("_id", mConsignmentItem._id + "");
                Log.e("name", mConsignmentItem.name);
                Log.e("sellingPrice", mConsignmentItem.sellingPrice);
                Log.e("purchasePrice", mConsignmentItem.purchasePrice);
                Log.e("countInStock", mConsignmentItem.countInStock);
                Log.e("stockStatus", mConsignmentItem.stockStatus);
                Log.e("lastModified", mConsignmentItem.lastModified);

                Intent mIntent = new Intent(getActivity(), InsertIConsignmentItemActivity.class);
                mIntent.putExtra("TYPE", "UPDATE");
                mIntent.putExtra("DATA", mConsignmentItem);
                startActivity(mIntent);

                break;
            case R.id.pmnuDelete:
                // Toast.makeText(getActivity(), "You clicked: " + item.getTitle(), Toast.LENGTH_SHORT).show();

                // get the _id of the item from the clicked list item
                Integer _id = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_ITEMS));
                //String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));
                Log.e("_id", _id + "");

                sqliteHelper.deleteConsignmentItem(_id);

                Toast.makeText(getActivity(), "Deleted Success", Toast.LENGTH_SHORT).show();
                refreshListData(itemSearchQuery);

                break;

            case R.id.pmnuShare:

                Integer _idConsignmentShare
                        = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_ITEMS));
                //String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));
                ConsignmentItem mConsignmentItemShare = sqliteHelper.getConsignmentItem(_idConsignmentShare);


                String shareBody = "Hi! checkout this Consignment information from my inventory" + "\n\n" +
                        "Consignment Name: " + mConsignmentItemShare.name + "\n" +
                        "Selling price: " + mConsignmentItemShare.sellingPrice + "\n" +
                        "Purchasing price: " + mConsignmentItemShare.purchasePrice + "\n" +
                        "Count in stock: " + mConsignmentItemShare.countInStock + "\n" +
                        "Stock status: " + mConsignmentItemShare.stockStatus + "\n" +
                        "Date acquired: " + Utils.getDate(Long.parseLong(mConsignmentItemShare.lastModified), "MM/dd/yyyy") + "\n";

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Consignment Info");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Please choose one!"));

                break;
        }

        return false;
    }
}