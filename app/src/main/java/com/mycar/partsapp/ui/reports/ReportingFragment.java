package com.mycar.partsapp.ui.reports;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.mycar.partsapp.InsertItemActivity;
import com.mycar.partsapp.R;
import com.mycar.partsapp.SqliteHelper;
import com.mycar.partsapp.Utils;
import com.mycar.partsapp.adapters.ItemsCursorAdapter;
import com.mycar.partsapp.models.Item;

public class ReportingFragment extends Fragment implements PopupMenu.OnMenuItemClickListener {

    ReportingViewModel reportingViewModel;

    ListView itemsListView;

    //Declaration SqliteHelper
    SqliteHelper sqliteHelper;

    ItemsCursorAdapter mItemsCursorAdapter;
    Cursor mCursor;
    Cursor selectedCursor;

    String itemSearchQuery = "";

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private TextView txtItemsCount;
    private TextView txtInventoryInvested;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        sqliteHelper = new SqliteHelper(getActivity());
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        reportingViewModel =
                ViewModelProviders.of(this).get(ReportingViewModel.class);
        View root = inflater.inflate(R.layout.fragment_share, container, false);
        itemsListView = root.findViewById(R.id.itemsListView);
        txtItemsCount = root.findViewById(R.id.txtItemsCount);
        txtInventoryInvested = root.findViewById(R.id.txtInventoryInvested);

        /*FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(getActivity(), InsertItemActivity.class);
                mIntent.putExtra("TYPE", "INSERT");
                startActivity(mIntent);

            }
        });*/

        itemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedCursor = (Cursor) itemsListView.getItemAtPosition(position);
                showPopup(view);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", "Called");
        refreshListData(itemSearchQuery);
    }

    private void refreshListData(String searchQuery) {

        mCursor = sqliteHelper.getAllItemsOutOfStock(searchQuery);
        mItemsCursorAdapter = new ItemsCursorAdapter(getActivity(), mCursor, 0);
        itemsListView.setAdapter(mItemsCursorAdapter);

        // Items Count
        String textTotalParts = "Total parts: " + sqliteHelper.getTotalPartsCount();
        txtItemsCount.setText(textTotalParts);

        // get Total purchase price
        String text = "Total investment: $" + sqliteHelper.getTotalPurchasingPrice();
        txtInventoryInvested.setText(text);
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v, Gravity.RIGHT);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            searchView.setQueryHint("Search Item Name");
            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);
                    itemSearchQuery = newText;
                    refreshListData(newText);

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    itemSearchQuery = query;
                    refreshListData(query);
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        //  String selectedItem = adapter.getItem(lvDays.getCheckedItemPosition());

        switch (item.getItemId()) {
            case R.id.pmnuEdit:
                // Toast.makeText(getActivity(), "You clicked: " + item.getTitle(), Toast.LENGTH_SHORT).show();

                Integer _idEdit = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_ITEMS));
                //String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));
                Item mItem = sqliteHelper.getItem(_idEdit);
                Log.e("_id", mItem._id + "");
                Log.e("name", mItem.name);
                Log.e("sellingPrice", mItem.sellingPrice);
                Log.e("purchasePrice", mItem.purchasePrice);
                Log.e("countInStock", mItem.countInStock);
                Log.e("stockStatus", mItem.stockStatus);
                Log.e("lastModified", mItem.lastModified);

                Intent mIntent = new Intent(getActivity(), InsertItemActivity.class);
                mIntent.putExtra("TYPE", "UPDATE");
                mIntent.putExtra("DATA", mItem);
                startActivity(mIntent);

                break;
            case R.id.pmnuDelete:
                // Toast.makeText(getActivity(), "You clicked: " + item.getTitle(), Toast.LENGTH_SHORT).show();

                // get the _id of the item from the clicked list item
                Integer _id = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_ITEMS));
                //String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));
                Log.e("_id", _id + "");

                sqliteHelper.deleteItem(_id);

                Toast.makeText(getActivity(), "Deleted Success", Toast.LENGTH_SHORT).show();
                refreshListData(itemSearchQuery);

                break;

            case R.id.pmnuShare:

                Integer _idItemShare
                        = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_ITEMS));
                //String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));
                Item mItemShare = sqliteHelper.getItem(_idItemShare);


                String shareBody = "Hi! checkout this item information from my inventory" + "\n\n" +
                        "Item Name: " + mItemShare.name + "\n" +
                        "Selling price: " + mItemShare.sellingPrice + "\n" +
                        "Purchasing price: " + mItemShare.purchasePrice + "\n" +
                        "Count in stock: " + mItemShare.countInStock + "\n" +
                        "Stock status: " + mItemShare.stockStatus + "\n" +
                        "Date acquired: " + Utils.getDate(Long.parseLong(mItemShare.lastModified), "MM/dd/yyyy") + "\n";

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Inventory Item Info");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Please choose one!"));

                break;
        }

        return false;
    }
}