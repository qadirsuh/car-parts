package com.mycar.partsapp.ui.reports;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ReportingViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ReportingViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Share fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}