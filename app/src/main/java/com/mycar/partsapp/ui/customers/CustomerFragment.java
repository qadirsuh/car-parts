package com.mycar.partsapp.ui.customers;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mycar.partsapp.InsertCustomerActivity;
import com.mycar.partsapp.InsertItemActivity;
import com.mycar.partsapp.R;
import com.mycar.partsapp.SqliteHelper;
import com.mycar.partsapp.Utils;
import com.mycar.partsapp.adapters.CustomerCursorAdapter;
import com.mycar.partsapp.adapters.ItemsCursorAdapter;
import com.mycar.partsapp.models.Customer;
import com.mycar.partsapp.models.Item;

public class CustomerFragment extends Fragment implements PopupMenu.OnMenuItemClickListener {

    private CustomerViewModel customerViewModel;

    //Declaration SqliteHelper
    SqliteHelper sqliteHelper;

    ListView customersListView;

    CustomerCursorAdapter mCustomerCursorAdapter;
    Cursor mCursor;
    Cursor selectedCursor;

    String itemSearchQuery = "";

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private TextView txtItemsCount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        sqliteHelper = new SqliteHelper(getActivity());
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        customerViewModel =
                ViewModelProviders.of(this).get(CustomerViewModel.class);
        View root = inflater.inflate(R.layout.fragment_customer, container, false);

        customersListView = root.findViewById(R.id.customersListView);
        txtItemsCount = root.findViewById(R.id.txtCustomerCount);

        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(getActivity(), InsertCustomerActivity.class);
                mIntent.putExtra("TYPE", "INSERT");
                startActivity(mIntent);

            }
        });

        customersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedCursor = (Cursor) customersListView.getItemAtPosition(position);
                showPopup(view);
            }
        });


        /*final TextView textView = root.findViewById(R.id.text_share);
        customerViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/


        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshListData(itemSearchQuery);
    }

    private void refreshListData(String searchQuery) {

        mCursor = sqliteHelper.getAllCustomers(searchQuery);
        mCustomerCursorAdapter = new CustomerCursorAdapter(getActivity(), mCursor, 0);
        customersListView.setAdapter(mCustomerCursorAdapter);

        // Items Count
        if (mCursor != null) {
            txtItemsCount.setText("Total Customers: " + mCursor.getCount());
        } else {
            txtItemsCount.setText("No customers found!");
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            searchView.setQueryHint("Search Customer Name");
            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);
                    itemSearchQuery = newText;
                    refreshListData(newText);

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    itemSearchQuery = query;
                    refreshListData(query);
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v, Gravity.RIGHT);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        //  String selectedItem = adapter.getItem(lvDays.getCheckedItemPosition());

        switch (item.getItemId()) {
            case R.id.pmnuEdit:
                // Toast.makeText(getActivity(), "You clicked: " + item.getTitle(), Toast.LENGTH_SHORT).show();

                Integer _idEdit = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_CUSTOMERS));
                Customer mCustomer = sqliteHelper.getCustomer(_idEdit);

                Log.e("_id", _idEdit + "");

                Log.e("name", mCustomer.name);
                Log.e("email", mCustomer.email);
                Log.e("phone", mCustomer.phoneNumber);

                Intent mIntent = new Intent(getActivity(), InsertCustomerActivity.class);
                mIntent.putExtra("TYPE", "UPDATE");
                mIntent.putExtra("DATA", mCustomer);
                startActivity(mIntent);

                break;
            case R.id.pmnuDelete:
                // Toast.makeText(getActivity(), "You clicked: " + item.getTitle(), Toast.LENGTH_SHORT).show();

                // get the _id of the item from the clicked list item
                Integer _idDelete = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_CUSTOMERS));
                //String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));
                Log.e("_idDelete", _idDelete + "");

                sqliteHelper.deleteCustomer(_idDelete);

                Toast.makeText(getActivity(), "Deleted Success", Toast.LENGTH_SHORT).show();
                refreshListData(itemSearchQuery);

                break;

            case R.id.pmnuShare:

                Integer _idItemShare
                        = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_ITEMS));

                Log.e("_idItemShare", _idItemShare + "");
                // String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));

                Customer mCustomerShare = sqliteHelper.getCustomer(_idItemShare);

                String shareBody = "Hi! checkout this customer information" + "\n\n" +
                        "Customer Name: " + mCustomerShare.name + "\n" +
                        "Email: " + mCustomerShare.email + "\n" +
                        "Phone: " + mCustomerShare.phoneNumber + "\n";

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Customer Info");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Please choose one!"));

                break;
        }

        return false;
    }
}