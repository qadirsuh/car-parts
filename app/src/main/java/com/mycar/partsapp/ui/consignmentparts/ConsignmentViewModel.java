package com.mycar.partsapp.ui.consignmentparts;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ConsignmentViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ConsignmentViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Consignment Parts fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}