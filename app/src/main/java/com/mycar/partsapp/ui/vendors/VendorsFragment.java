package com.mycar.partsapp.ui.vendors;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mycar.partsapp.InsertVendorActivity;
import com.mycar.partsapp.R;
import com.mycar.partsapp.SqliteHelper;
import com.mycar.partsapp.adapters.CustomerCursorAdapter;
import com.mycar.partsapp.adapters.VendorCursorAdapter;
import com.mycar.partsapp.models.Vendor;

public class VendorsFragment extends Fragment implements PopupMenu.OnMenuItemClickListener {

    private VendorsViewModel vendorsViewModel;

    //Declaration SqliteHelper
    SqliteHelper sqliteHelper;

    ListView vendorsListView;

    VendorCursorAdapter mVendorCursorAdapter;
    Cursor mCursor;
    Cursor selectedCursor;

    String itemSearchQuery = "";

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private TextView txtItemsCount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        sqliteHelper = new SqliteHelper(getActivity());
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        vendorsViewModel =
                ViewModelProviders.of(this).get(VendorsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_vendors, container, false);

        vendorsListView = root.findViewById(R.id.vendorsListView);
        txtItemsCount = root.findViewById(R.id.txtVendorsCount);

        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(getActivity(), InsertVendorActivity.class);
                mIntent.putExtra("TYPE", "INSERT");
                startActivity(mIntent);

            }
        });

        vendorsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedCursor = (Cursor) vendorsListView.getItemAtPosition(position);
                showPopup(view);
            }
        });


        /*final TextView textView = root.findViewById(R.id.text_share);
        VendorViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/


        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshListData(itemSearchQuery);
    }

    private void refreshListData(String searchQuery) {

        mCursor = sqliteHelper.getAllVendors(searchQuery);
        mVendorCursorAdapter = new VendorCursorAdapter(getActivity(), mCursor, 0);
        vendorsListView.setAdapter(mVendorCursorAdapter);

        // Items Count
        if (mCursor != null) {
            txtItemsCount.setText("Total Vendors: " + mCursor.getCount());
        } else {
            txtItemsCount.setText("No Vendors found!");
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            searchView.setQueryHint("Search Vendor Name");
            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);
                    itemSearchQuery = newText;
                    refreshListData(newText);

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    itemSearchQuery = query;
                    refreshListData(query);
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v, Gravity.RIGHT);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {

        //  String selectedItem = adapter.getItem(lvDays.getCheckedItemPosition());

        switch (item.getItemId()) {
            case R.id.pmnuEdit:
                // Toast.makeText(getActivity(), "You clicked: " + item.getTitle(), Toast.LENGTH_SHORT).show();

                Integer _idEdit = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_VENDORS));
                Vendor mVendor = sqliteHelper.getVendor(_idEdit);

                Log.e("_id", _idEdit + "");

                Log.e("name", mVendor.name);
                Log.e("email", mVendor.email);
                Log.e("phone", mVendor.phoneNumber);

                Intent mIntent = new Intent(getActivity(), InsertVendorActivity.class);
                mIntent.putExtra("TYPE", "UPDATE");
                mIntent.putExtra("DATA", mVendor);
                startActivity(mIntent);

                break;
            case R.id.pmnuDelete:
                // Toast.makeText(getActivity(), "You clicked: " + item.getTitle(), Toast.LENGTH_SHORT).show();

                // get the _id of the item from the clicked list item
                Integer _idDelete = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_VENDORS));
                //String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));
                Log.e("_idDelete", _idDelete + "");

                sqliteHelper.deleteVendor(_idDelete);

                Toast.makeText(getActivity(), "Deleted Success", Toast.LENGTH_SHORT).show();
                refreshListData(itemSearchQuery);

                break;

            case R.id.pmnuShare:

                Integer _idItemShare
                        = selectedCursor.getInt(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ID_ITEMS));

                Log.e("_idItemShare", _idItemShare + "");
                // String name = selectedCursor.getString(selectedCursor.getColumnIndexOrThrow(SqliteHelper.KEY_ITEM_NAME));

                Vendor mVendorShare = sqliteHelper.getVendor(_idItemShare);

                String shareBody = "Hi! checkout this Vendor information" + "\n\n" +
                        "Vendor Name: " + mVendorShare.name + "\n" +
                        "Email: " + mVendorShare.email + "\n" +
                        "Phone: " + mVendorShare.phoneNumber + "\n";

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Vendor Info");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Please choose one!"));

                break;
        }

        return false;
    }
}