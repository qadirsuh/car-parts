package com.mycar.partsapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.mycar.partsapp.R;
import com.mycar.partsapp.SqliteHelper;
import com.mycar.partsapp.Utils;

public class ConsignmentCursorAdapter extends CursorAdapter {

    private LayoutInflater cursorInflater;

    public ConsignmentCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);

        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // R.layout.list_row is your xml layout for each row
        return cursorInflater.inflate(R.layout.carparts_recyclerview_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView textViewTitle = view.findViewById(R.id.txt_item_name);
        TextView txt_item_selling_price = view.findViewById(R.id.txt_item_selling_price);
        TextView txt_item_purchasing_price = view.findViewById(R.id.txt_item_purchasing_price);
        TextView txt_item_stock_in_count = view.findViewById(R.id.txt_item_stock_in_count);
        TextView txt_item_date_acquired = view.findViewById(R.id.txt_item_date_acquired);
        TextView txt_item_in_stock = view.findViewById(R.id.txt_item_in_stock);

        String title = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_ITEM_NAME));
        String sellingPrice = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_ITEM_SELLING_PRICE));
        String purchasingPrice = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_ITEM_PURCHASING_PRICE));
        String stockInCount = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_ITEM_COUNT_IN_STOCK));
        String lastUpdate = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_ITEM_LAST_MODIFIED));
        String stockStatus = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_ITEM_STOCK_STATUS));

//        String formattedDate = Utils.getDate(Long.parseLong(lastUpdate), "MM/dd/yyyy hh:mm:ss");
         String formattedDate = Utils.getDate(Long.parseLong(lastUpdate), "MM/dd/yyyy");

        textViewTitle.setText(title);
        txt_item_selling_price.setText("$" + sellingPrice);
        txt_item_purchasing_price.setText("$" + purchasingPrice);
        txt_item_stock_in_count.setText(stockInCount);
        txt_item_date_acquired.setText("" + formattedDate);
        txt_item_in_stock.setText("(" + stockStatus + ")");

        if (stockStatus.equalsIgnoreCase("In-Stock")) {
            txt_item_in_stock.setTextColor(Color.parseColor("#51C95D"));
        } else {
            txt_item_in_stock.setTextColor(Color.parseColor("#FC0000"));
        }

    }
}
