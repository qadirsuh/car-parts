package com.mycar.partsapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.mycar.partsapp.R;
import com.mycar.partsapp.SqliteHelper;

public class VendorCursorAdapter extends CursorAdapter {

    private LayoutInflater cursorInflater;

    public VendorCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);

        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // R.layout.list_row is your xml layout for each row
        return cursorInflater.inflate(R.layout.customers_listview_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView txtName = view.findViewById(R.id.txtName);
        TextView txtEmail = view.findViewById(R.id.txtEmail);
        TextView txtPhone = view.findViewById(R.id.txtPhone);

        String name = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_VENDOR_NAME));
        String email = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_VENDOR_EMAIL));
        String phone = cursor.getString(cursor.getColumnIndex(SqliteHelper.KEY_VENDOR_PHONE));

        txtName.setText(name);
        txtEmail.setText(email);
        txtPhone.setText(phone);

    }
}
