package com.mycar.partsapp;

import android.content.Context;
import android.content.SharedPreferences;

import com.mycar.partsapp.models.User;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utils {

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static void saveUserLoggedIn(Context mContext, User mUser) {

        SharedPreferences prefs = mContext.getSharedPreferences(
                "PREF", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("USER_NAME", mUser.userName);
        editor.putString("USER_EMAIL", mUser.email);
        editor.putString("USER_PASSWORD", mUser.password);
        editor.putString("USER_ID", mUser.id);
        editor.apply();

    }

    public static User getUserLoggedIn(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(
                "PREF", Context.MODE_PRIVATE);

        // use a default value using new Date()
        String USER_NAME = prefs.getString("USER_NAME", null);
        if (USER_NAME != null) {

            String USER_ID = prefs.getString("USER_ID", null);
            String USER_EMAIL = prefs.getString("USER_EMAIL", null);
            String USER_PASSWORD = prefs.getString("USER_PASSWORD", null);

            User mUser = new User(USER_ID, USER_NAME, USER_EMAIL, USER_PASSWORD);
            return mUser;
        }

        return null;
    }

}
