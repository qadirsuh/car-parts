package com.mycar.partsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mycar.partsapp.models.Customer;
import com.mycar.partsapp.models.Item;

public class InsertCustomerActivity extends AppCompatActivity {

    EditText editTextCustomerName, editTextCustomerEmail, editTextCustomerPhone;
    Button buttonAddCustomer, buttonCancel;

    // Declaration SqliteHelper
    SqliteHelper sqliteHelper;

    Intent mIntent;
    Integer customerIdToUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_customer);

        mIntent = getIntent();

        sqliteHelper = new SqliteHelper(this);

        editTextCustomerName = findViewById(R.id.editTextCustomerName);
        editTextCustomerEmail = findViewById(R.id.editTextCustomerEmail);
        editTextCustomerPhone = findViewById(R.id.editTextCustomerPhone);

        buttonAddCustomer = findViewById(R.id.buttonAddCustomer);
        buttonCancel = findViewById(R.id.buttonCancel);

        // Check if EDIT/UPDATE option selected
        if (mIntent.getStringExtra("TYPE").equalsIgnoreCase("UPDATE")) {

            // Set the values
            Customer mCustomer = (Customer) mIntent.getSerializableExtra("DATA");
            customerIdToUpdate = mCustomer._id;
            if (mCustomer != null) {
                editTextCustomerName.setText(mCustomer.name);
                editTextCustomerEmail.setText(mCustomer.email);
                editTextCustomerPhone.setText(mCustomer.phoneNumber);
            }

            buttonAddCustomer.setText("Update");
        }


        buttonAddCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = editTextCustomerName.getText().toString();
                String email = editTextCustomerEmail.getText().toString();
                String phoneNumber = editTextCustomerPhone.getText().toString();

                if (name.isEmpty()) {
                    editTextCustomerName.setError("Required field");
                } else if (email.isEmpty()) {
                    editTextCustomerEmail.setError("Required field");
                } else if (phoneNumber.isEmpty()) {
                    editTextCustomerPhone.setError("Required field");
                } else {

                    Customer mCustomer = new Customer(name, email, phoneNumber);

                    // Check insert / Update operation
                    if (mIntent.getStringExtra("TYPE").equalsIgnoreCase("INSERT")) {

                        sqliteHelper.addCustomer(mCustomer);
                        Toast.makeText(InsertCustomerActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {

                        mCustomer.setId(customerIdToUpdate);
                        sqliteHelper.updateCustomer(mCustomer);

                        Toast.makeText(InsertCustomerActivity.this, "Update Success", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                }
            }
        });

    }
}
