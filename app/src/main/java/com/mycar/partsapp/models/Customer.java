package com.mycar.partsapp.models;

import java.io.Serializable;

public class Customer implements Serializable {

    public Integer _id;
    public String name;
    public String email;
    public String phoneNumber;

    public Customer(String name, String email, String phoneNumber) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public void setId(Integer _id) {
        this._id = _id;
    }
}
