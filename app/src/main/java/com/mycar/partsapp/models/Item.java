package com.mycar.partsapp.models;

import java.io.Serializable;

public class Item implements Serializable {

    public Integer _id;
    public String name;
    public String sellingPrice;
    public String purchasePrice;
    public String countInStock;
    public String stockStatus;
    public String lastModified;

    public Item(String name, String sellingPrice, String purchasePrice, String countInStock, String stockStatus, String lastModified) {
        this.name = name;
        this.sellingPrice = sellingPrice;
        this.purchasePrice = purchasePrice;
        this.countInStock = countInStock;
        this.stockStatus = stockStatus;
        this.lastModified = lastModified;
    }

    public void setId(Integer _id) {
        this._id = _id;
    }
}
