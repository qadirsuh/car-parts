package com.mycar.partsapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mycar.partsapp.models.ConsignmentItem;

public class InsertIConsignmentItemActivity extends AppCompatActivity {

    EditText editTextConsignmentItemName;
    EditText editTextSellingPrice;
    EditText editTextPurchasePrice;
    EditText editTextCountInStock;
    Spinner spinnerStockStatus;
    Button buttonAddConsignmentItem;
    Button buttonCancel;

    // Declaration SqliteHelper
    SqliteHelper sqliteHelper;

    Intent mIntent;
    Integer ConsignmentItemIdToUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_consignment_item);

        mIntent = getIntent();

        sqliteHelper = new SqliteHelper(this);

        editTextConsignmentItemName = findViewById(R.id.editTextConsignmentName);
        editTextSellingPrice = findViewById(R.id.editTextSellingPrice);
        editTextPurchasePrice = findViewById(R.id.editTextPurchasePrice);
        editTextCountInStock = findViewById(R.id.editTextCountInStock);
        spinnerStockStatus = findViewById(R.id.spinnerStockStatus);
        buttonAddConsignmentItem = findViewById(R.id.buttonAddItem);
        buttonCancel = findViewById(R.id.buttonCancel);

        // Check if EDIT/UPDATE option selected
        if (mIntent.getStringExtra("TYPE").equalsIgnoreCase("UPDATE")) {

            // Set the values
            ConsignmentItem mConsignmentItem = (ConsignmentItem) mIntent.getSerializableExtra("DATA");
            ConsignmentItemIdToUpdate = mConsignmentItem._id;
            if (mConsignmentItem != null) {
                editTextConsignmentItemName.setText(mConsignmentItem.name);
                editTextSellingPrice.setText(mConsignmentItem.sellingPrice);
                editTextPurchasePrice.setText(mConsignmentItem.purchasePrice);
                editTextCountInStock.setText(mConsignmentItem.countInStock);
                if (mConsignmentItem.stockStatus.equalsIgnoreCase("In-Stock")) {
                    spinnerStockStatus.setSelection(0);
                } else {
                    spinnerStockStatus.setSelection(1);
                }
            }

            buttonAddConsignmentItem.setText("Update");
        }

        buttonAddConsignmentItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ConsignmentItemName = editTextConsignmentItemName.getText().toString();
                String ConsignmentItemSellingPrice = editTextSellingPrice.getText().toString();
                String ConsignmentItemPurchasePrice = editTextPurchasePrice.getText().toString();
                String ConsignmentItemCountInStock = editTextCountInStock.getText().toString();
                String stockStatus = spinnerStockStatus.getSelectedItem().toString();

                if (ConsignmentItemName.isEmpty()) {
                    editTextConsignmentItemName.setError("Required field");
                } else if (ConsignmentItemSellingPrice.isEmpty()) {
                    editTextSellingPrice.setError("Required field");
                } else if (ConsignmentItemPurchasePrice.isEmpty()) {
                    editTextPurchasePrice.setError("Required field");
                } else if (ConsignmentItemCountInStock.isEmpty()) {
                    editTextCountInStock.setError("Required field");
                } else {

                    // Check insert / Update operation
                    if (mIntent.getStringExtra("TYPE").equalsIgnoreCase("INSERT")) {

                        ConsignmentItem ConsignmentItem = new ConsignmentItem(ConsignmentItemName, ConsignmentItemSellingPrice, ConsignmentItemPurchasePrice, ConsignmentItemCountInStock, stockStatus, System.currentTimeMillis() + "");
                        sqliteHelper.addConsignmentItem(ConsignmentItem);

                        Toast.makeText(InsertIConsignmentItemActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        finish();

                    } else {

                        ConsignmentItem ConsignmentItem = new ConsignmentItem(ConsignmentItemName, ConsignmentItemSellingPrice, ConsignmentItemPurchasePrice, ConsignmentItemCountInStock, stockStatus, System.currentTimeMillis() + "");
                        ConsignmentItem.setId(ConsignmentItemIdToUpdate);
                        sqliteHelper.updateConsignmentItem(ConsignmentItem);

                        Toast.makeText(InsertIConsignmentItemActivity.this, "Update Success", Toast.LENGTH_SHORT).show();

                        if (Long.parseLong(ConsignmentItemCountInStock) <= 1) {
                            Toast.makeText(InsertIConsignmentItemActivity.this, "Low in stock!", Toast.LENGTH_SHORT).show();
                        }

                        finish();
                    }
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
