package com.mycar.partsapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mycar.partsapp.models.ConsignmentItem;
import com.mycar.partsapp.models.Customer;
import com.mycar.partsapp.models.Item;
import com.mycar.partsapp.models.User;
import com.mycar.partsapp.models.Vendor;


public class SqliteHelper extends SQLiteOpenHelper {

    //DATABASE NAME
    public static final String DATABASE_NAME = "CarParts.db";

    //DATABASE VERSION
    public static final int DATABASE_VERSION = 1;

    //TABLE NAME
    public static final String TABLE_USERS = "users";

    //TABLE USERS COLUMNS
    //ID COLUMN @primaryKey
    public static final String KEY_ID = "id";

    //COLUMN user name
    public static final String KEY_USER_NAME = "username";

    //COLUMN email
    public static final String KEY_EMAIL = "email";

    //COLUMN password
    public static final String KEY_PASSWORD = "password";


    //TABLE items
    public static final String TABLE_ITEMS = "items";

    //TABLE items
    public static final String TABLE_CONSIGNMENTS = "consignments";

    //TABLE ITEMS COLUMNS
    //ID COLUMN @primaryKey
    public static final String KEY_ID_ITEMS = "_id";

    //COLUMN name
    public static final String KEY_ITEM_NAME = "name";

    //COLUMN selling_price
    public static final String KEY_ITEM_SELLING_PRICE = "selling_price";

    //COLUMN purchase_price
    public static final String KEY_ITEM_PURCHASING_PRICE = "purchasing_price";

    //COLUMN count_in_stock
    public static final String KEY_ITEM_COUNT_IN_STOCK = "count_in_stock";

    //COLUMN stock_status
    public static final String KEY_ITEM_STOCK_STATUS = "stock_status";

    //COLUMN stock_status
    public static final String KEY_ITEM_LAST_MODIFIED = "last_modified";

    //TABLE customers
    public static final String TABLE_CUSTOMER = "customers";

    //ID COLUMN @primaryKey
    public static final String KEY_ID_CUSTOMERS = "_id";

    //COLUMN customer_name
    public static final String KEY_CUSTOMER_NAME = "customer_name";

    //COLUMN customer_email
    public static final String KEY_CUSTOMER_EMAIL = "customer_email";

    //COLUMN customer_phone
    public static final String KEY_CUSTOMER_PHONE = "customer_phone";

    //TABLE customers
    public static final String TABLE_VENDORS = "vendors";

    //ID COLUMN @primaryKey
    public static final String KEY_ID_VENDORS = "_id";

    //COLUMN vendor_name
    public static final String KEY_VENDOR_NAME = "vendor_name";

    //COLUMN vendor_email
    public static final String KEY_VENDOR_EMAIL = "vendor_email";

    //COLUMN vendor_phone
    public static final String KEY_VENDOR_PHONE = "vendor_phone";


    //SQL for creating users table
    public static final String SQL_TABLE_USERS = " CREATE TABLE " + TABLE_USERS
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_USER_NAME + " TEXT, "
            + KEY_EMAIL + " TEXT, "
            + KEY_PASSWORD + " TEXT"
            + " ) ";

    //SQL for creating customers table
    public static final String SQL_TABLE_CUSTOMERS = " CREATE TABLE " + TABLE_CUSTOMER
            + " ( "
            + KEY_ID_CUSTOMERS + " INTEGER PRIMARY KEY, "
            + KEY_CUSTOMER_NAME + " TEXT, "
            + KEY_CUSTOMER_EMAIL + " TEXT, "
            + KEY_CUSTOMER_PHONE + " TEXT"
            + " ) ";

    //SQL for creating vendors table
    public static final String SQL_TABLE_VENDORS = " CREATE TABLE " + TABLE_VENDORS
            + " ( "
            + KEY_ID_VENDORS + " INTEGER PRIMARY KEY, "
            + KEY_VENDOR_NAME + " TEXT, "
            + KEY_VENDOR_EMAIL + " TEXT, "
            + KEY_VENDOR_PHONE + " TEXT"
            + " ) ";

    //SQL for creating items table
    public static final String SQL_TABLE_ITEMS = " CREATE TABLE " + TABLE_ITEMS
            + " ( "
            + KEY_ID_ITEMS + " INTEGER PRIMARY KEY, "
            + KEY_ITEM_NAME + " TEXT, "
            + KEY_ITEM_SELLING_PRICE + " TEXT, "
            + KEY_ITEM_PURCHASING_PRICE + " TEXT, "
            + KEY_ITEM_COUNT_IN_STOCK + " TEXT, "
            + KEY_ITEM_STOCK_STATUS + " TEXT,"
            + KEY_ITEM_LAST_MODIFIED + " TEXT"
            + " ) ";

    //SQL for creating items table
    public static final String SQL_TABLE_CONSIGNMENT = " CREATE TABLE " + TABLE_CONSIGNMENTS
            + " ( "
            + KEY_ID_ITEMS + " INTEGER PRIMARY KEY, "
            + KEY_ITEM_NAME + " TEXT, "
            + KEY_ITEM_SELLING_PRICE + " TEXT, "
            + KEY_ITEM_PURCHASING_PRICE + " TEXT, "
            + KEY_ITEM_COUNT_IN_STOCK + " TEXT, "
            + KEY_ITEM_STOCK_STATUS + " TEXT,"
            + KEY_ITEM_LAST_MODIFIED + " TEXT"
            + " ) ";


    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Create Table when oncreate gets called
        sqLiteDatabase.execSQL(SQL_TABLE_USERS);
        sqLiteDatabase.execSQL(SQL_TABLE_ITEMS);
        sqLiteDatabase.execSQL(SQL_TABLE_CUSTOMERS);
        sqLiteDatabase.execSQL(SQL_TABLE_VENDORS);
        sqLiteDatabase.execSQL(SQL_TABLE_CONSIGNMENT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //drop table to create new one if database version updated
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_USERS);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_ITEMS);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_CUSTOMER);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_VENDORS);
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + SQL_TABLE_CONSIGNMENT);
    }

    // using this method we can add users to user table
    public void addUser(User user) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        //Put username in  @values
        values.put(KEY_USER_NAME, user.userName);

        //Put email in  @values
        values.put(KEY_EMAIL, user.email);

        //Put password in  @values
        values.put(KEY_PASSWORD, user.password);

        // insert row
        long user_id = db.insert(TABLE_USERS, null, values);
    }

    // using this method we can delete the item from table by its _id
    public void deleteItem(Integer _id) {
        // get writable database
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete query
        db.delete(TABLE_ITEMS, KEY_ID_ITEMS + "=?", new String[]{_id + ""});
    }

    // using this method we can delete the item from table by its _id
    public void deleteConsignmentItem(Integer _id) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONSIGNMENTS, KEY_ID_ITEMS + "=?", new String[]{_id + ""});
    }

    public void deleteCustomer(Integer _id) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_CUSTOMER, KEY_ID_CUSTOMERS + "=?", new String[]{_id + ""});
    }

    public void deleteVendor(Integer _id) {
        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_VENDORS, KEY_ID_VENDORS + "=?", new String[]{_id + ""});
    }

    public void addConsignmentItem(ConsignmentItem item) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        //Put name in  @values
        values.put(KEY_ITEM_NAME, item.name);

        //Put sellingPrice in  @values
        values.put(KEY_ITEM_SELLING_PRICE, item.sellingPrice);

        //Put purchasePrice in  @values
        values.put(KEY_ITEM_PURCHASING_PRICE, item.purchasePrice);

        //Put countInStock in  @values
        values.put(KEY_ITEM_COUNT_IN_STOCK, item.countInStock);

        //Put stockStatus in  @values
        values.put(KEY_ITEM_STOCK_STATUS, item.stockStatus);

        //Put lastModified in  @values
        values.put(KEY_ITEM_LAST_MODIFIED, item.lastModified);

        // insert row
        long item_id = db.insert(TABLE_CONSIGNMENTS, null, values);

    }

    // using this method we can add items to item table
    public void addItem(Item item) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        //Put name in  @values
        values.put(KEY_ITEM_NAME, item.name);

        //Put sellingPrice in  @values
        values.put(KEY_ITEM_SELLING_PRICE, item.sellingPrice);

        //Put purchasePrice in  @values
        values.put(KEY_ITEM_PURCHASING_PRICE, item.purchasePrice);

        //Put countInStock in  @values
        values.put(KEY_ITEM_COUNT_IN_STOCK, item.countInStock);

        //Put stockStatus in  @values
        values.put(KEY_ITEM_STOCK_STATUS, item.stockStatus);

        //Put lastModified in  @values
        values.put(KEY_ITEM_LAST_MODIFIED, item.lastModified);

        // insert row
        long item_id = db.insert(TABLE_ITEMS, null, values);
    }

    public void addVendor(Vendor mVendor) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        values.put(KEY_VENDOR_NAME, mVendor.name);
        values.put(KEY_VENDOR_EMAIL, mVendor.email);
        values.put(KEY_VENDOR_PHONE, mVendor.phoneNumber);

        // insert row
        long item_id = db.insert(TABLE_VENDORS, null, values);

    }

    public void addCustomer(Customer customer) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        values.put(KEY_CUSTOMER_NAME, customer.name);
        values.put(KEY_CUSTOMER_EMAIL, customer.email);
        values.put(KEY_CUSTOMER_PHONE, customer.phoneNumber);

        // insert row
        long item_id = db.insert(TABLE_CUSTOMER, null, values);
    }

    public void updateConsignmentItem(ConsignmentItem item) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        //Put name in  @values
        values.put(KEY_ITEM_NAME, item.name);

        //Put sellingPrice in  @values
        values.put(KEY_ITEM_SELLING_PRICE, item.sellingPrice);

        //Put purchasePrice in  @values
        values.put(KEY_ITEM_PURCHASING_PRICE, item.purchasePrice);

        //Put countInStock in  @values
        values.put(KEY_ITEM_COUNT_IN_STOCK, item.countInStock);

        //Put stockStatus in  @values
        values.put(KEY_ITEM_STOCK_STATUS, item.stockStatus);

        //Put lastModified in  @values
        values.put(KEY_ITEM_LAST_MODIFIED, item.lastModified);

        // update row
        long item_id = db.update(TABLE_CONSIGNMENTS, values, "_id=" + item._id, null);

    }

    // using this method we can update items to item table
    public void updateItem(Item item) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        //create content values to insert
        ContentValues values = new ContentValues();

        //Put name in  @values
        values.put(KEY_ITEM_NAME, item.name);

        //Put sellingPrice in  @values
        values.put(KEY_ITEM_SELLING_PRICE, item.sellingPrice);

        //Put purchasePrice in  @values
        values.put(KEY_ITEM_PURCHASING_PRICE, item.purchasePrice);

        //Put countInStock in  @values
        values.put(KEY_ITEM_COUNT_IN_STOCK, item.countInStock);

        //Put stockStatus in  @values
        values.put(KEY_ITEM_STOCK_STATUS, item.stockStatus);

        //Put lastModified in  @values
        values.put(KEY_ITEM_LAST_MODIFIED, item.lastModified);

        // update row
        long item_id = db.update(TABLE_ITEMS, values, "_id=" + item._id, null);
    }

    public void updateVendor(Vendor mVendor) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_VENDOR_NAME, mVendor.name);
        values.put(KEY_VENDOR_EMAIL, mVendor.email);
        values.put(KEY_VENDOR_PHONE, mVendor.phoneNumber);

        // update row
        long item_id = db.update(TABLE_VENDORS, values, "_id=" + mVendor._id, null);

    }

    // using this method we can update items to item table
    public void updateCustomer(Customer mCustomer) {

        //get writable database
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_CUSTOMER_NAME, mCustomer.name);
        values.put(KEY_CUSTOMER_EMAIL, mCustomer.email);
        values.put(KEY_CUSTOMER_PHONE, mCustomer.phoneNumber);

        // update row
        long item_id = db.update(TABLE_CUSTOMER, values, "_id=" + mCustomer._id, null);
    }

    public Item getItem(Integer _id) {

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_ITEMS + " WHERE " + KEY_ID_ITEMS + " = " + _id + "";
        Cursor mCursor = db.rawQuery(query, null);

        if (mCursor.moveToFirst()) {

            Item mItem = new Item(mCursor.getString(1), mCursor.getString(2),
                    mCursor.getString(3), mCursor.getString(4), mCursor.getString(5), mCursor.getString(6));
            mItem.setId(_id);
            return mItem;
        }

        return null;
    }

    public ConsignmentItem getConsignmentItem(Integer _id) {

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONSIGNMENTS + " WHERE " + KEY_ID_ITEMS + " = " + _id + "";
        Cursor mCursor = db.rawQuery(query, null);

        if (mCursor.moveToFirst()) {

            ConsignmentItem mItem = new ConsignmentItem(mCursor.getString(1), mCursor.getString(2),
                    mCursor.getString(3), mCursor.getString(4), mCursor.getString(5), mCursor.getString(6));
            mItem.setId(_id);
            return mItem;
        }

        return null;
    }

    public Customer getCustomer(Integer _id) {

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CUSTOMER + " WHERE " + KEY_ID_CUSTOMERS + " = " + _id + "";
        Cursor mCursor = db.rawQuery(query, null);

        if (mCursor.moveToFirst()) {

            Customer mItem = new Customer(mCursor.getString(1), mCursor.getString(2),
                    mCursor.getString(3));
            mItem.setId(_id);
            return mItem;
        }

        return null;
    }

    public Vendor getVendor(Integer _id) {


        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_VENDORS + " WHERE " + KEY_ID_VENDORS + " = " + _id + "";
        Cursor mCursor = db.rawQuery(query, null);

        if (mCursor.moveToFirst()) {

            Vendor mVendor = new Vendor(mCursor.getString(1), mCursor.getString(2),
                    mCursor.getString(3));
            mVendor.setId(_id);
            return mVendor;
        }

        return null;

    }

    public User Authenticate(User user) {
        // Create SQLiteDatabase object
        SQLiteDatabase db = this.getReadableDatabase();

        // Make sqlite db query via db.query helper method
        // i.e SELECT * FROM users WHERE email = 'sherry@gmail.com'
        // This will return a cursor object
        Cursor cursor = db.query(TABLE_USERS,// Selecting Table
                // Selecting columns want to query
                new String[]{KEY_ID, KEY_USER_NAME, KEY_EMAIL, KEY_PASSWORD},
                KEY_EMAIL + "=?", // Where clause
                new String[]{user.email}, // Where clause
                null, null, null);

        // Check if cursor is not null and do not have zero rows.
        if (cursor != null && cursor.moveToFirst()) {
            //if cursor has value then in user database there is user associated with this given email
            User user1 = new User(cursor.getString(0), cursor.getString(1),
                    cursor.getString(2), cursor.getString(3));

            // Check the users password with password stored in database
            if (user.password.equalsIgnoreCase(user1.password)) {
                return user1;
            }
        }

        // if user password does not matches or there is no record with that email then return @false
        return null;
    }

    public Cursor getAllItems(String searchTerm) {
        // Create SQLiteDatabase object
        SQLiteDatabase db = this.getReadableDatabase();
        // Select all columns
        String[] columns = {KEY_ID_ITEMS, KEY_ITEM_NAME,
                KEY_ITEM_SELLING_PRICE, KEY_ITEM_PURCHASING_PRICE,
                KEY_ITEM_COUNT_IN_STOCK, KEY_ITEM_STOCK_STATUS,
                KEY_ITEM_LAST_MODIFIED};

        Cursor cursor = null;

        // Check if searchTerm is not null and length > 0
        if (searchTerm != null && searchTerm.length() > 0) {

            // Search all items names matching with string
            // i.e
            // SELECT * FROM items WHERE name LIKE '%sample%' ORDER BY _id DESC
            String sql = "SELECT * FROM " + TABLE_ITEMS + " WHERE " + KEY_ITEM_NAME
                    + " LIKE '%" + searchTerm + "%' ORDER BY " + KEY_ID_ITEMS + " DESC";

            cursor = db.rawQuery(sql, null);

        } else {
            // Else Show all Items
            // SELECT * FROM items ORDER BY _id DESC
            cursor = db.query(TABLE_ITEMS, columns, null, null,
                    null, null, KEY_ID_ITEMS + " DESC");
        }
        // Check if cursor is not null and has some Rows to display
        if (cursor != null && cursor.moveToFirst()) {
            //if cursor has value return that cursor else no items found yet
            return cursor;
        }
        //if cursor is null or zero items
        return null;
    }

    public Cursor getAllItemsOutOfStock(String searchTerm) {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = {KEY_ID_ITEMS, KEY_ITEM_NAME, KEY_ITEM_SELLING_PRICE, KEY_ITEM_PURCHASING_PRICE, KEY_ITEM_COUNT_IN_STOCK, KEY_ITEM_STOCK_STATUS, KEY_ITEM_LAST_MODIFIED};
        Cursor cursor = null;

        if (searchTerm != null && searchTerm.length() > 0) {
            String sql = "SELECT * FROM " + TABLE_ITEMS + " WHERE " + KEY_ITEM_STOCK_STATUS + " LIKE '%out-of-stock%' AND " + KEY_ITEM_NAME + " LIKE '%" + searchTerm + "%' ORDER BY " + KEY_ID_ITEMS + " DESC";
            Log.e("sql if", sql);
            cursor = db.rawQuery(sql, null);

        } else {

            String sql = "SELECT * FROM " + TABLE_ITEMS + " WHERE " + KEY_ITEM_STOCK_STATUS + " LIKE '%out-of-stock%' ORDER BY " + KEY_ID_ITEMS + " DESC";
            Log.e("sql else", sql);
            cursor = db.rawQuery(sql, null);

        }

        if (cursor != null && cursor.moveToFirst()) {
            //if cursor has value return that cursor else no items found yet
            return cursor;
        }

        //if cursor is null or zero items
        return null;
    }

    public Integer getTotalPurchasingPrice() {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT SUM(" + KEY_ITEM_PURCHASING_PRICE + ") as Total FROM " + TABLE_ITEMS, null);

        if (cursor.moveToFirst()) {
            return cursor.getInt(cursor.getColumnIndex("Total"));// get final total
        }

        return 0;
    }

    public Integer getTotalPartsCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT SUM(" + KEY_ITEM_COUNT_IN_STOCK + ") as Total FROM " + TABLE_ITEMS, null);

        if (cursor.moveToFirst()) {
            return cursor.getInt(cursor.getColumnIndex("Total"));// get final total
        }

        return 0;
    }

    public Integer getTotalConsignmentPartsCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT SUM(" + KEY_ITEM_COUNT_IN_STOCK + ") as Total FROM " + TABLE_CONSIGNMENTS, null);

        if (cursor.moveToFirst()) {
            return cursor.getInt(cursor.getColumnIndex("Total"));// get final total
        }

        return 0;
    }

    public Cursor getAllConsignments(String searchTerm) {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = {KEY_ID_ITEMS, KEY_ITEM_NAME, KEY_ITEM_SELLING_PRICE, KEY_ITEM_PURCHASING_PRICE, KEY_ITEM_COUNT_IN_STOCK, KEY_ITEM_STOCK_STATUS, KEY_ITEM_LAST_MODIFIED};
        Cursor cursor = null;

        if (searchTerm != null && searchTerm.length() > 0) {
            String sql = "SELECT * FROM " + TABLE_CONSIGNMENTS + " WHERE " + KEY_ITEM_NAME + " LIKE '%" + searchTerm + "%' ORDER BY " + KEY_ID_ITEMS + " DESC";
            Log.e("sql", sql);
            cursor = db.rawQuery(sql, null);

        } else {
            cursor = db.query(TABLE_CONSIGNMENTS, columns, null, null, null, null, KEY_ID_ITEMS + " DESC");
        }

        if (cursor != null && cursor.moveToFirst()) {
            //if cursor has value return that cursor else no items found yet
            return cursor;
        }

        //if cursor is null or zero items
        return null;
    }

    public Cursor getAllVendors(String searchQuery) {

        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = {KEY_ID_VENDORS, KEY_VENDOR_NAME, KEY_VENDOR_EMAIL, KEY_VENDOR_PHONE};
        Cursor cursor = null;

        if (searchQuery != null && searchQuery.length() > 0) {
            String sql = "SELECT * FROM " + TABLE_VENDORS + " WHERE " + KEY_VENDOR_NAME + " LIKE '%" + searchQuery + "%' ORDER BY " + KEY_ID_VENDORS + " DESC";
            Log.e("sql", sql);
            cursor = db.rawQuery(sql, null);

        } else {
            cursor = db.query(TABLE_VENDORS, columns, null, null, null, null, KEY_ID_VENDORS + " DESC");
        }

        if (cursor != null && cursor.moveToFirst()) {
            //if cursor has value return that cursor else no items found yet
            return cursor;
        }

        //if cursor is null or zero items
        return null;

    }

    public Cursor getAllCustomers(String searchTerm) {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = {KEY_ID_CUSTOMERS, KEY_CUSTOMER_NAME, KEY_CUSTOMER_EMAIL, KEY_CUSTOMER_PHONE};
        Cursor cursor = null;

        if (searchTerm != null && searchTerm.length() > 0) {
            String sql = "SELECT * FROM " + TABLE_CUSTOMER + " WHERE " + KEY_CUSTOMER_NAME + " LIKE '%" + searchTerm + "%' ORDER BY " + KEY_ID_CUSTOMERS + " DESC";
            Log.e("sql", sql);
            cursor = db.rawQuery(sql, null);

        } else {
            cursor = db.query(TABLE_CUSTOMER, columns, null, null, null, null, KEY_ID_CUSTOMERS + " DESC");
        }

        if (cursor != null && cursor.moveToFirst()) {
            //if cursor has value return that cursor else no items found yet
            return cursor;
        }

        //if cursor is null or zero items
        return null;
    }

    public boolean isEmailExists(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USERS,// Selecting Table
                new String[]{KEY_ID, KEY_USER_NAME, KEY_EMAIL, KEY_PASSWORD},//Selecting columns want to query
                KEY_EMAIL + "=?",
                new String[]{email},//Where clause
                null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            //if cursor has value then in user database there is user associated with this given email so return true
            return true;
        }

        //if email does not exist return false
        return false;
    }
}
