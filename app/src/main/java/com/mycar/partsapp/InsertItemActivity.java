package com.mycar.partsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mycar.partsapp.models.Item;

import java.util.Date;

public class InsertItemActivity extends AppCompatActivity {

    EditText editTextItemName;
    EditText editTextSellingPrice;
    EditText editTextPurchasePrice;
    EditText editTextCountInStock;
    Spinner spinnerStockStatus;
    Button buttonAddItem;
    Button buttonCancel;

    // Declaration SqliteHelper
    SqliteHelper sqliteHelper;

    Intent mIntent;
    Integer itemIdToUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_item);

        mIntent = getIntent();

        sqliteHelper = new SqliteHelper(this);

        editTextItemName = findViewById(R.id.editTextItemName);
        editTextSellingPrice = findViewById(R.id.editTextSellingPrice);
        editTextPurchasePrice = findViewById(R.id.editTextPurchasePrice);
        editTextCountInStock = findViewById(R.id.editTextCountInStock);
        spinnerStockStatus = findViewById(R.id.spinnerStockStatus);
        buttonAddItem = findViewById(R.id.buttonAddItem);
        buttonCancel = findViewById(R.id.buttonCancel);

        // Check if EDIT/UPDATE option selected
        if (mIntent.getStringExtra("TYPE").equalsIgnoreCase("UPDATE")) {

            // Set the values
            Item mItem = (Item) mIntent.getSerializableExtra("DATA");
            itemIdToUpdate = mItem._id;
            if (mItem != null) {
                editTextItemName.setText(mItem.name);
                editTextSellingPrice.setText(mItem.sellingPrice);
                editTextPurchasePrice.setText(mItem.purchasePrice);
                editTextCountInStock.setText(mItem.countInStock);
                if (mItem.stockStatus.equalsIgnoreCase("In-Stock")) {
                    spinnerStockStatus.setSelection(0);
                } else {
                    spinnerStockStatus.setSelection(1);
                }
            }

            buttonAddItem.setText("Update Item");
        }

        buttonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String itemName = editTextItemName.getText().toString();
                String itemSellingPrice = editTextSellingPrice.getText().toString();
                String itemPurchasePrice = editTextPurchasePrice.getText().toString();
                String itemCountInStock = editTextCountInStock.getText().toString();
                String stockStatus = spinnerStockStatus.getSelectedItem().toString();

                if (itemName.isEmpty()) {
                    editTextItemName.setError("Required field");
                } else if (itemSellingPrice.isEmpty()) {
                    editTextSellingPrice.setError("Required field");
                } else if (itemPurchasePrice.isEmpty()) {
                    editTextPurchasePrice.setError("Required field");
                } else if (itemCountInStock.isEmpty()) {
                    editTextCountInStock.setError("Required field");
                } else {

                    // Check insert / Update operation
                    if (mIntent.getStringExtra("TYPE").equalsIgnoreCase("INSERT")) {

                        Item item = new Item(itemName, itemSellingPrice, itemPurchasePrice, itemCountInStock, stockStatus, System.currentTimeMillis() + "");
                        sqliteHelper.addItem(item);

                        Toast.makeText(InsertItemActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        finish();

                    } else {

                        Item item = new Item(itemName, itemSellingPrice, itemPurchasePrice, itemCountInStock, stockStatus, System.currentTimeMillis() + "");
                        item.setId(itemIdToUpdate);
                        sqliteHelper.updateItem(item);

                        Toast.makeText(InsertItemActivity.this, "Update Success", Toast.LENGTH_SHORT).show();

                        if (Long.parseLong(itemCountInStock) <= 1) {
                            Toast.makeText(InsertItemActivity.this, "Low in stock!", Toast.LENGTH_SHORT).show();
                        }

                        finish();
                    }
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
