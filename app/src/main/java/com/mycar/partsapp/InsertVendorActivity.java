package com.mycar.partsapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mycar.partsapp.models.Vendor;

public class InsertVendorActivity extends AppCompatActivity {

    EditText editTextVendorName, editTextVendorEmail, editTextVendorPhone;
    Button buttonAddVendor, buttonCancel;

    // Declaration SqliteHelper
    SqliteHelper sqliteHelper;

    Intent mIntent;
    Integer VendorIdToUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_vendor);

        mIntent = getIntent();

        sqliteHelper = new SqliteHelper(this);

        editTextVendorName = findViewById(R.id.editTextVendorName);
        editTextVendorEmail = findViewById(R.id.editTextVendorEmail);
        editTextVendorPhone = findViewById(R.id.editTextVendorPhone);

        buttonAddVendor = findViewById(R.id.buttonAddVendor);
        buttonCancel = findViewById(R.id.buttonCancel);

        // Check if EDIT/UPDATE option selected
        if (mIntent.getStringExtra("TYPE").equalsIgnoreCase("UPDATE")) {

            // Set the values
            Vendor mVendor = (Vendor) mIntent.getSerializableExtra("DATA");
            VendorIdToUpdate = mVendor._id;
            if (mVendor != null) {
                editTextVendorName.setText(mVendor.name);
                editTextVendorEmail.setText(mVendor.email);
                editTextVendorPhone.setText(mVendor.phoneNumber);
            }

            buttonAddVendor.setText("Update");
        }


        buttonAddVendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = editTextVendorName.getText().toString();
                String email = editTextVendorEmail.getText().toString();
                String phoneNumber = editTextVendorPhone.getText().toString();

                if (name.isEmpty()) {
                    editTextVendorName.setError("Required field");
                } else if (email.isEmpty()) {
                    editTextVendorEmail.setError("Required field");
                } else if (phoneNumber.isEmpty()) {
                    editTextVendorPhone.setError("Required field");
                } else {

                    Vendor mVendor = new Vendor(name, email, phoneNumber);

                    // Check insert / Update operation
                    if (mIntent.getStringExtra("TYPE").equalsIgnoreCase("INSERT")) {

                        sqliteHelper.addVendor(mVendor);
                        Toast.makeText(InsertVendorActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {

                        mVendor.setId(VendorIdToUpdate);
                        sqliteHelper.updateVendor(mVendor);

                        Toast.makeText(InsertVendorActivity.this, "Update Success", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                }
            }
        });

    }
}
